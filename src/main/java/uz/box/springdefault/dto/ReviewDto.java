package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class ReviewDto {

    private Long id;
    private String author;
    private String message;
    private Integer rating;
    private String createdAt;

}
