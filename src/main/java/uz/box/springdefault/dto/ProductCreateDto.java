package uz.box.springdefault.dto;

import lombok.Data;
import uz.box.springdefault.domain.Category;
import uz.box.springdefault.domain.ResourceFile;
import uz.box.springdefault.domain.Review;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductCreateDto {

    private String title;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private Integer year;
    private String country;
    private String description;
    private BigDecimal price;
    private Double rating;
    private Long mainPhoto;
    private Long category;
    private List<Long> resourceFiles;
    private List<ReviewCreateDto> reviews;

}
