package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class CategoryUpdateDto {

    private Long id;
    private Long parentId;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;

}
