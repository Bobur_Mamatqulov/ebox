package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class ResourceFileUpdateDto {

    private Long id;
    private String name;

}
