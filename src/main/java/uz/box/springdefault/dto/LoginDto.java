package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class LoginDto {

    private String username;
    private String password;
}
