package uz.box.springdefault.dto;

import lombok.Data;
import uz.box.springdefault.domain.OrderItem;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderDto {

    private Long id;
    private BigDecimal totalPrice;
    private String firstName;
    private String lastName;
    private String city;
    private String address;
    private String email;
    private String longitude;
    private String latitude;
    private List<OrderItemDto> orderItems;

}
