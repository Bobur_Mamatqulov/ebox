package uz.box.springdefault.dto;

import lombok.Data;
import uz.box.springdefault.domain.Product;
import uz.box.springdefault.domain.Role;

import java.util.List;

@Data
public class UserDto {

    private String firstName;
    private String lastName;
    private String middleName;
    private String phoneNumber;
    private String password;
    private String email;
    private String activationCode;
    private String passwordResetCode;
    private boolean active;
    private String provider;
    private List<Role> roles;
    private List<Product> favourites;


}
