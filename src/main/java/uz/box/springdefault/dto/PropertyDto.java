package uz.box.springdefault.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PropertyDto {

    private Long id;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private String valueUz;
    private String valueRu;
    private String valueEn;
    private Long productId;
}
