package uz.box.springdefault.dto;

import lombok.Data;
import uz.box.springdefault.domain.Product;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderItemDto {

    private Long id;
    private BigDecimal amount;
    private Integer quantity;
    private Long productId;
}
