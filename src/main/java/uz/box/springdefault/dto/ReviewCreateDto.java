package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class ReviewCreateDto {

    private String author;
    private String message;
    private Integer rating;
    private String createdAt;

}
