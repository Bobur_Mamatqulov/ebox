package uz.box.springdefault.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItemCreateDto {

    private BigDecimal amount;
    private Integer quantity;
    private Long productId;
}
