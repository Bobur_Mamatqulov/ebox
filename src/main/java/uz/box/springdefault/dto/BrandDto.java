package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class BrandDto {

    private Long id;
    private String name;
}
