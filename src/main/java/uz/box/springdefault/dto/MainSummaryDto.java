//package uz.box.springdefault.dto;
//
//import lombok.AllArgsConstructor;
//import lombok.Data;
//import lombok.NoArgsConstructor;
//
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.Id;
//import java.math.BigDecimal;
//
///**
// * Author: Ulug'bek Ro'zimboyev  <ulugbekrozimboyev@gmail.com>
// * Date: 6/15/2021 6:18 PM
// */
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//@Entity
//public class MainSummaryDto {
//
//    @Id
//    @Column(name = "all_count", nullable = false)
//    private Long allCount;
//
//    @Column(name = "com_count")
//    private Long comCount;
//
//    @Column(name = "find_amount")
//    private BigDecimal findAmount;
//
//    @Column(name = "payed_amount")
//    private BigDecimal payedAmount;
//
//    @Column(name = "valid_amount")
//    private BigDecimal validAmount;
//
//    @Column(name = "performance")
//    private BigDecimal performance;
//
//    @Column(name = "perform_sum")
//    private BigDecimal performSum;
//}
