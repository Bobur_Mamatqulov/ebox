package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class BrandUpdateDto {

    private Long id;
    private String name;
}
