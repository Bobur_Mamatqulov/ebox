package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class PropertyCreateDto {

    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private String valueUz;
    private String valueRu;
    private String valueEn;
    private Long productId;
}
