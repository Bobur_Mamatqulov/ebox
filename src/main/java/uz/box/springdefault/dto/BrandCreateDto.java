package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class BrandCreateDto {

    private String name;
}
