package uz.box.springdefault.dto;

import lombok.Data;
import uz.box.springdefault.domain.*;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductDto {

    private String title;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private Integer year;
    private String country;
    private String description;
    private BigDecimal price;
    private Double rating;
    private ResourceFileDto mainPhoto;
    private CategoryDto category;
    private List<ResourceFileDto> resourceFiles;
    private List<ReviewDto> reviews;

}
