package uz.box.springdefault.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class ProductUpdateDto {

    private String title;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private Integer year;
    private String country;
    private String description;
    private BigDecimal price;
    private Double rating;
    private Long mainPhoto;
    private Long category;
    private List<Long> resourceFiles;
    private List<ReviewUpdateDto> reviews;

}
