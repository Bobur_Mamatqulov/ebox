//package uz.box.springdefault.dto;
//
//import com.fasterxml.jackson.annotation.JsonFormat;
//import lombok.Data;
//import lombok.EqualsAndHashCode;
//import lombok.ToString;
//
//import java.util.Date;
//
///**
// * Author: Bobur M
// * Date: 6/30/2021 16:43 AM
// */
//@Data
//@EqualsAndHashCode
//@ToString
//public class BiCompaniesDto {
//
//    private String tin;
//    private String shortName;
//    private String name;
//    private String inspectionName;
//    private String address;
//    private Long status;
//    private Long taxMode;
//    private Long taxpayerType;
//    private Long soato;
//    private String oked;
//    private String okedName;
//    private String vatNum;
//
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy", timezone = "Asia/Tashkent")
//    private Date activeDate;
//
//}
