package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class CategoryCreateDto {

    private Long parentId;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;

}
