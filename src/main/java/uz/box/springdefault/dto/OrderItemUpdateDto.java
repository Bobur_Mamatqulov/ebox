package uz.box.springdefault.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItemUpdateDto {

    private Long id;
    private BigDecimal amount;
    private Integer quantity;
    private Long productId;
}
