package uz.box.springdefault.dto;

import lombok.Data;

@Data
public class ResourceFileCreateDto {

    private String name;
    private String url;
    private String mimeType;
    private Long size;
    private boolean isDefault;


}

