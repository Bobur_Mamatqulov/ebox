package uz.box.springdefault.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JwtResponseDto {
    private String tokenType="Bearer";
    private String tokenBody;

    public JwtResponseDto(String tokenBody) {
        this.tokenBody = tokenBody;
    }
}
