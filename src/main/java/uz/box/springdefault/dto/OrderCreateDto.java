package uz.box.springdefault.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class OrderCreateDto {

    private BigDecimal totalPrice;
    private String firstName;
    private String lastName;
    private String city;
    private String address;
    private String email;
    private String longitude;
    private String latitude;
    private List<OrderItemCreateDto> orderItems;

}
