package uz.box.springdefault.dto;

import lombok.*;

import java.time.LocalDateTime;


@Data
public class ResourceFileDto {

    private final String now = LocalDateTime.now().toString();
    private Long id;
    private String name;
    private String url;
    private String mimeType;
    private Long size;
    private boolean isDefault;
    private String createdAt;

}

