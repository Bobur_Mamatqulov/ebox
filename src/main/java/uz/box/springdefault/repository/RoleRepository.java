package uz.box.springdefault.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.box.springdefault.domain.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
