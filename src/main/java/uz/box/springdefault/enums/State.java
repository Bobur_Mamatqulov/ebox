package uz.box.springdefault.enums;

public enum State {
    NEW,
    UPDATED,
    DELETED,
    IN_PROGRESS
}
