package uz.box.springdefault.enums;

public enum AuthProvider {
    LOCAL, GOOGLE, FACEBOOK
}
