package uz.box.springdefault.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "properties")
public class Property extends AbstractEntity {

    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private String valueUz;
    private String valueRu;
    private String valueEn;

    @ManyToOne
    private Product product;



}
