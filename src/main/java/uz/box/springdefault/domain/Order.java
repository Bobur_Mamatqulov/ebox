package uz.box.springdefault.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order extends AbstractEntity {

    private BigDecimal totalPrice;
    private String firstName;
    private String lastName;
    private String city;
    private String address;
    private String email;
    private String longitude;
    private String latitude;

    @OneToMany(fetch = FetchType.EAGER)
    private List<OrderItem> orderItem;

    }
