package uz.box.springdefault.domain;
import lombok.*;
import org.hibernate.annotations.Where;
import uz.box.springdefault.enums.State;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Where(clause = "state <> 2")
@Table(name = "resource_files")
public class ResourceFile extends AbstractEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    @Column(name = "state", columnDefinition = "NUMERIC default 0")
    private State state = State.NEW;

    @Column(name = "file_name")
    private String name;

    @Column(name = "url")
    private String url;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "file_size")
    private Long size;

    @Column(name = "is_default")
    private Boolean isDefault;

}
