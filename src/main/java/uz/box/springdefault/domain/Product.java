package uz.box.springdefault.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import uz.box.springdefault.enums.AuthProvider;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "products")
public class Product extends AbstractEntity {

    private String title;
    private String name;
    private String nameUz;
    private String nameRu;
    private String nameEn;
    private Integer year;
    private String country;
    private String description;
    private BigDecimal price;
    private Double rating;

    @OneToOne
    private ResourceFile mainPhoto;

    @ManyToOne
    private Category category;

    @ManyToOne
    private Brand brand;

    @OneToMany
    private List<ResourceFile> resourceFiles;

    @OneToMany
    private List<Review> reviews;

}
