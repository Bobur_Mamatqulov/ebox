package uz.box.springdefault.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@Entity
@Table(name = "reviews")
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long id;

    private String author;
    private String message;
    private Integer rating;
    private LocalDate createdAt;

    public Review() {
        this.createdAt = LocalDate.now();
    }
}
