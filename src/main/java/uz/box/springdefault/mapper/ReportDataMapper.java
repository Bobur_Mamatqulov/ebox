//package uz.box.springdefault.mapper;
//
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import uz.sicnt.bi.cameralapi.domain.ReportDataItem;
//import uz.sicnt.bi.cameralapi.dto.ReportDataItemDto;
//
///**
// * Author: Ulug'bek Ro'zimboyev  <ulugbekrozimboyev@gmail.com>
// * Date: 6/14/2021 3:57 PM
// */
//@Mapper(componentModel = "spring", uses = {ReportTypeMapper.class})
//public interface ReportDataMapper extends EntityMapper<ReportDataItemDto, ReportDataItem> {
//
//    @Mapping(source = "reportType.id", target = "reportTypeId")
//    @Mapping(source = "reportType.name", target = "reportTypeName")
//    ReportDataItemDto toDto(ReportDataItem entity);
//}
