package uz.box.springdefault.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.box.springdefault.domain.User;
import uz.box.springdefault.repository.RoleRepository;
import uz.box.springdefault.repository.UserRepository;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(
                    new User(
                            "admin",
                            "admin",
                            "+998998612042",
                            passwordEncoder.encode("123456789"),
                            roleRepository.findAll()
                    )
            );
        }
    }
}
